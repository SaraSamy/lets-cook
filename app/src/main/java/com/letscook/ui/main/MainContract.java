package com.letscook.ui.main;

import com.letscook.ui.base.MvpPresenter;
import com.letscook.ui.base.MvpView;

public interface MainContract {

    public interface View extends MvpView {
    }

    public interface Presenter extends MvpPresenter {
    }
}

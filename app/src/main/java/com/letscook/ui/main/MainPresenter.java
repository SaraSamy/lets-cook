package com.letscook.ui.main;
import com.letscook.data.DataManager;
import com.letscook.ui.base.BasePresenter;

public class MainPresenter extends BasePresenter implements MainContract.Presenter {
    public MainPresenter(DataManager dataManager) {
        super(dataManager);
    }
}

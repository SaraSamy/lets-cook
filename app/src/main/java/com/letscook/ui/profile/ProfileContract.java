package com.letscook.ui.profile;

import com.letscook.model.User;
import com.letscook.ui.base.MvpPresenter;
import com.letscook.ui.base.MvpView;

public interface ProfileContract {
     interface View extends MvpView {
        void onSuccess(User user);
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
        void getUser();
        void update(User user, Boolean flag);
    }
}

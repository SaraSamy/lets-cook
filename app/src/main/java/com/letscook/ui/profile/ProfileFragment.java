package com.letscook.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.letscook.R;
import com.letscook.data.AppDataManager;
import com.letscook.model.User;
import com.letscook.ui.base.BaseFragment;
import com.letscook.ui.login.LoginActivity;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import kotlin.jvm.internal.Intrinsics;

public class ProfileFragment extends BaseFragment implements ProfileContract.View {
    @BindView(R.id.userIV)
    ImageView userIV;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.logoutB)
    Button logoutB;
    private ProfilePresenter presenter;
    @BindView(R.id.setting)
    ImageView setting;
    private User user;
    private Boolean flag = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void setUp(View view) {
        presenter = new ProfilePresenter(new AppDataManager());
        presenter.onAttach(this);
        presenter.getUser();
    }


    public static ProfileFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new ProfileFragment();
    }

    @Override
    public void onSuccess(User userBack) {
        user = userBack;
        View view = getView();
        if (view != null) {
            setting.setOnClickListener(view1 -> {
                SettingBS();
            });
            if (user.getImg() != null) {
                Glide.with(this).load(user.getImg()).into(userIV);
            }
            nameTV.setText(user.getName());

            if (user.getPhone() != null) {
                phone.setText(user.getPhone());
            }
            if (user.getEmail() != null) {
                email.setText(user.getEmail());
            }
            view.findViewById(R.id.logoutB).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("letsCook", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.apply();

                    FirebaseAuth.getInstance().signOut();
                    Intent logout = new Intent(getContext(), LoginActivity.class);
                    logout.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(logout);
                    getActivity().finish();

                }
            });
        }
    }

    void SettingBS() {
//        DialogSheet dialogSheet = new DialogSheet(Objects.requireNonNull(getContext()));
//        dialogSheet.setTitle("Update profile")
//                .setColoredNavigationBar(true)
//                .setView(R.layout.bottom_sheet_edit_profile)
//                .setTitleTextSize(20)
//                .setButtonsColorRes(R.color.colorAccent)
//                .show();
//        View inflatedView = dialogSheet.getInflatedView();
//        Button add = inflatedView.findViewById(R.id.addB);
//        ImageView usernewIV = inflatedView.findViewById(R.id.userIV);
//        ImageView imageChooser = inflatedView.findViewById(R.id.imageChooser);
//        EditText nameET = inflatedView.findViewById(R.id.nameET);
//        EditText PhoneET = inflatedView.findViewById(R.id.PhoneET);
//        nameET.setText(user.getName());
//        PhoneET.setText(user.getPhone());
//        if (user.getImg() != null) {
//            Glide.with(this).load(user.getImg()).into(usernewIV);
//        }
//        imageChooser.setOnClickListener(view -> {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.CAMERA)
//                        != PackageManager.PERMISSION_GRANTED &&
//
//                        checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.READ_EXTERNAL_STORAGE)
//                                != PackageManager.PERMISSION_GRANTED &&
//
//                        checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                                != PackageManager.PERMISSION_GRANTED) {
//
//                    requestPermissions(new String[]{Manifest.permission.CAMERA,
//                                    Manifest.permission.READ_EXTERNAL_STORAGE,
//                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            100);
//                    return;
//                }
//            }
//            TedRxBottomPicker.with(getActivity())
//                    .show()
//                    .subscribe(uri -> {
//                                user.setImg(uri.toString());
//                                flag = true;
//                                Glide.with(this)
//                                        .load(uri)
//                                        .into(usernewIV);
//                            },
//                            throwable -> {
//                            });
//        });
//        add.setOnClickListener(view -> {
//            if (nameET.getText().toString().isEmpty()) {
//                nameET.setError("Name is required");
//                nameET.requestFocus();
//                return;
//            }
//            if (PhoneET.getText().toString().isEmpty()) {
//                PhoneET.setError("Phone is required");
//                PhoneET.requestFocus();
//                return;
//            } else {
//                user.setName(nameET.getText().toString());
//                user.setPhone(PhoneET.getText().toString());
//                presenter.update(user, flag);
//                dialogSheet.dismiss();
//            }
//        });
    }
}

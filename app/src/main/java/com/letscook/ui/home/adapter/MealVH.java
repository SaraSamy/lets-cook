package com.letscook.ui.home.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.letscook.R;
import com.letscook.model.Meal;

import butterknife.BindView;
import butterknife.ButterKnife;


class MealVH extends RecyclerView.ViewHolder {

    @BindView(R.id.mealTV)
    TextView titleTV;
    @BindView(R.id.mealIV)
    ImageView mealIV;

    MealVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind( Meal meal) {
        titleTV.setText(meal.getName());
        Glide.with(itemView.getContext()).load(meal.getImg()).into(mealIV);
    }
}

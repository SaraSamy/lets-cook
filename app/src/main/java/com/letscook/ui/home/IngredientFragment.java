package com.letscook.ui.home;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.letscook.R;
import com.letscook.model.Meal;
import com.letscook.ui.base.BaseFragment;
import com.letscook.ui.home.adapter.MealsAdapter;
import com.letscook.utils.ViewUtils;
import com.marcoscg.dialogsheet.DialogSheet;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;
import gun0912.tedbottompicker.TedRxBottomPicker;
import kotlin.jvm.internal.Intrinsics;

import static android.content.Context.MODE_PRIVATE;
import static androidx.core.content.ContextCompat.checkSelfPermission;

public class IngredientFragment extends BaseFragment implements IngrAdapter.IngrInterface {
    IngrAdapter adapter;
    IngrAdapter storeAdapterdapter;
    @BindView(R.id.ingRV)
    RecyclerView ingRV;
    RecyclerView recyclerView;
    @BindView(R.id.addFab)
    FloatingActionButton addFab;

    boolean isAdded = false;


    private static final int MY_PERMISSIONS_REQUEST = 100;
    String image = "https://firebasestorage.googleapis.com/v0/b/findmeal-f3c90.appspot.com/o/vegetable.png?alt=media&token=6a3521ed-cf91-4c36-815e-0b43013f3f74";
    private ArrayList<Meal> ings = new ArrayList<>();
    private ArrayList<Meal> myStore = new ArrayList<>();
    ImageView ingIV;


    public IngredientFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ingredient_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SharedPreferences prefs = Objects.requireNonNull(getActivity()).getSharedPreferences("letsCook", MODE_PRIVATE);
        String type = prefs.getString("type", "type");
        if (type.equals("admin")) {
            FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference()
                    .child("ings").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                        ings.clear();
                        for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                            Meal meal = childDataSnapshot.getValue(Meal.class);
                            meal.setId(childDataSnapshot.getKey());
                            ings.add(meal);
                        }
                        storeAdapterdapter.addIngrs(ings);
                    } else
                        Log.e("ings", "empty");
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else {
            FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference()
                    .child("store").child(FirebaseAuth.getInstance().getUid()).addValueEventListener(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue() != null) {
                                ings.clear();
                                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                    Meal meal = childDataSnapshot.getValue(Meal.class);
                                    meal.setId(childDataSnapshot.getKey());
                                    ings.add(meal);
                                }
                                storeAdapterdapter.addIngrs(ings);
                            } else
                                Log.e("ings", "empty");
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        }

    }

    @Override
    protected void setUp(View view) {
        adapter = new IngrAdapter(myStore, this::ingrInterface);
        storeAdapterdapter = new IngrAdapter(ings, this::ingrInterface);
        setUpRecyclers(ingRV);
        addFab.setOnClickListener(view1 -> {
            SharedPreferences prefs = Objects.requireNonNull(getActivity()).getSharedPreferences("letsCook", MODE_PRIVATE);
            String type = prefs.getString("type", "type");
            if (type.equals("admin")) {
                addIngBS();
            } else {
                getBaseActivity().showLoading();
                FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference()
                        .child("ings").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            myStore.clear();
                            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                Meal meal = childDataSnapshot.getValue(Meal.class);
                                meal.setId(childDataSnapshot.getKey());
                                myStore.add(meal);
                            }
                            adapter.addIngrs(myStore);
                            getBaseActivity().hideLoading();
                            chooseIngItems();
                        } else
                            Log.e("ings", "empty");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }

        });

    }

    public static IngredientFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new IngredientFragment();
    }

    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(storeAdapterdapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(2, ViewUtils.dpToPx(4.0F), true)));
    }

    void chooseIngItems() {
        isAdded = true;
        DialogSheet dialogSheet = new DialogSheet(Objects.requireNonNull(getContext()));
        dialogSheet.setTitle("Add ingredients")
                .setColoredNavigationBar(true)
                .setView(R.layout.add_store_bottom_sheet)
                .setTitleTextSize(20)
                .setButtonsColorRes(R.color.colorAccent)
                .show();
        View inflatedView = dialogSheet.getInflatedView();
        Button add = inflatedView.findViewById(R.id.addB);
        recyclerView = inflatedView.findViewById(R.id.ingRV);
        GridLayoutManager girdLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(2, ViewUtils.dpToPx(4.0F), true)));

        add.setOnClickListener(view -> {
            isAdded = false;
            dialogSheet.dismiss();
            startActivity(SugMealsActivity.getStartIntent(getContext()));
        });

    }

    void deleteIng(Meal meal) {
        DialogSheet dialogSheet = new DialogSheet(Objects.requireNonNull(getContext()));
        dialogSheet.setTitle("Attention")
                .setColoredNavigationBar(true)
                .setView(R.layout.delete_bottom_sheet)
                .setTitleTextSize(20)
                .setButtonsColorRes(R.color.colorAccent)
                .show();
        View inflatedView = dialogSheet.getInflatedView();
        Button delete = inflatedView.findViewById(R.id.addB);


        SharedPreferences prefs = Objects.requireNonNull(getActivity()).getSharedPreferences("letsCook", MODE_PRIVATE);
        String type = prefs.getString("type", "type");


        delete.setOnClickListener(view -> {
            getBaseActivity().showLoading();
            if (!type.equals("admin")) {
                FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference().child("store")
                        .child(FirebaseAuth.getInstance().getUid()).child(meal.getId()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        getBaseActivity().hideLoading();
                        dialogSheet.dismiss();
                    }
                });

            }else{
                FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference().child("ings")
                        .child(meal.getId()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        getBaseActivity().hideLoading();
                        dialogSheet.dismiss();
                    }
                });
            }
        });

    }

    void addIngBS() {
        DialogSheet dialogSheet = new DialogSheet(Objects.requireNonNull(getContext()));
        dialogSheet.setTitle("Add ingredients")
                .setColoredNavigationBar(true)
                .setView(R.layout.add_ing_bottom_sheet)
                .setTitleTextSize(20)
                .setButtonsColorRes(R.color.colorAccent)
                .show();
        View inflatedView = dialogSheet.getInflatedView();
        Button add = inflatedView.findViewById(R.id.addB);
        ingIV = inflatedView.findViewById(R.id.ingIV);
        ImageView imageChooser = inflatedView.findViewById(R.id.imageChooser);
        TextView name = inflatedView.findViewById(R.id.nameET);
        imageChooser.setOnClickListener(view -> {
            getImage();
            Glide.with(this)
                    .load(image)
                    .into(ingIV);
        });

        add.setOnClickListener(view -> {

            getBaseActivity().showLoading();
            if (name.getText().toString().isEmpty()) {
                name.setError("ingredient name is required");
                name.requestFocus();
                return;
            } else
                FirebaseStorage.getInstance()
                        .getReference(System.currentTimeMillis() + ".jpg").putFile(Uri.parse(image)).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        taskSnapshot.getMetadata()
                                .getReference()
                                .getDownloadUrl()
                                .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                           @Override
                                                           public void onComplete(@NonNull Task<Uri> task) {
                                                               FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference()
                                                                       .child("ings").push().setValue(
                                                                       new Meal(
                                                                               name.getText().toString(),
                                                                               null,
                                                                               Objects.requireNonNull(task.getResult()).toString(),
                                                                               null
                                                                       )
                                                               ).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                   @Override
                                                                   public void onComplete(@NonNull Task<Void> task) {
                                                                       getBaseActivity().hideLoading();
                                                                       Toast.makeText(getContext(), "added", Toast.LENGTH_SHORT).show();

                                                                   }
                                                               });
                                                           }
                                                       }
                                );
                    }
                });


            dialogSheet.dismiss();

        });
    }


    private void getImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED &&

                    checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED &&

                    checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST);
                return;
            }
        }
        TedRxBottomPicker.with(getActivity())
                .show()

                .subscribe(uri -> {
                            image = uri.toString();
                            if (ingIV != null)
                                Glide.with(this)
                                        .load(image)
                                        .into(ingIV);
                        },
                        throwable -> {
                        });
    }

    @Override
    public void ingrInterface(Meal meal) {

        if (isAdded) {
            getBaseActivity().showLoading();
            FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference()
                    .child("store").child(FirebaseAuth.getInstance().getUid()).child(meal.getId()).setValue(meal).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    getBaseActivity().hideLoading();
                    Toast.makeText(getContext(), "store updated", Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            deleteIng(meal);
        }
    }
}

package com.letscook.ui.home;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.letscook.ui.base.BaseFragment;

import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentStatePagerAdapter   {
    private ArrayList<BaseFragment> fragments = new ArrayList<>();
    BaseFragment currentFragment;

    public ViewPagerAdapter(FragmentManager manager, ArrayList<BaseFragment> fragments, Context context){
        super(manager);
        this.fragments=fragments;
    }

    public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    public BaseFragment getCurrentFragment() {
        return currentFragment;
    }

    public void setCurrentFragment(BaseFragment currentFragment) {
        this.currentFragment = currentFragment;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        this.currentFragment=fragments.get(position);
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}

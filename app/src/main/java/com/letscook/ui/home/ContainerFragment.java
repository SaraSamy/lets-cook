package com.letscook.ui.home;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.duolingo.open.rtlviewpager.RtlViewPager;
import com.letscook.R;
import com.letscook.ui.base.BaseFragment;
import com.letscook.ui.profile.ProfileFragment;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

public class ContainerFragment extends BaseFragment {

    private ViewPagerAdapter viewPagerAdapter;
    private final ArrayList fragments = new ArrayList();
    private BaseFragment currentFragment;
    private AHBottomNavigation bottomNavigation;
    private RtlViewPager homePager;

    public ContainerFragment() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_container, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (bottomNavigation.getItemsCount() == 0) {

            AHBottomNavigationItem item1 = new AHBottomNavigationItem("Meals", R.drawable.dinner, R.color.white);
            AHBottomNavigationItem item2 = new AHBottomNavigationItem("Ingredients", R.drawable.flour, R.color.white);
            AHBottomNavigationItem item3 = new AHBottomNavigationItem("Profile", R.drawable.user, R.color.white);
            bottomNavigation.addItem(item1);
            bottomNavigation.addItem(item2);
            bottomNavigation.addItem(item3);
            bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));
            bottomNavigation.setBehaviorTranslationEnabled(false);
            bottomNavigation.setAccentColor(Color.parseColor("#328A35"));
            bottomNavigation.setInactiveColor(Color.parseColor("#747474"));
            bottomNavigation.setForceTint(true);
            bottomNavigation.setTranslucentNavigationEnabled(true);
            bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
            bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
            bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);
            bottomNavigation.setColored(false);
            bottomNavigation.setCurrentItem(0);
            bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));
            bottomNavigation.setItemDisableColor(Color.parseColor("#3A000000"));

            if (this.fragments.isEmpty()) {
                HomeFragment Meals = HomeFragment.newInstance("");
                this.fragments.add(Meals);
                IngredientFragment Home = IngredientFragment.newInstance("");
                this.fragments.add(Home);
                ProfileFragment profile = ProfileFragment.newInstance("");
                this.fragments.add(profile);

            }

            bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
                @Override
                public boolean onTabSelected(int position, boolean wasSelected) {

                    currentFragment = viewPagerAdapter.getCurrentFragment();
                    if (currentFragment == null) {
                        currentFragment = viewPagerAdapter.getCurrentFragment();
                    }

                    if (wasSelected) {
                        return true;
                    }

                    homePager.setCurrentItem(position, false);

                    if (currentFragment == null) {
                        return true;
                    }

                    currentFragment = viewPagerAdapter.getCurrentFragment();

                    return true;
                }
            });


            viewPagerAdapter = new ViewPagerAdapter(getFragmentManager(), fragments, getContext());
            homePager.setAdapter(viewPagerAdapter);

            homePager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    switch (position) {
                        case 0:
                            bottomNavigation.setCurrentItem(0);
                            break;
                        case 1:
                            bottomNavigation.setCurrentItem(1);
                            break;
                        case 2:
                            bottomNavigation.setCurrentItem(2);
                            break;

                    }
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }
    }

    @Override
    public void setUp(@Nullable View view) {
        assert view != null;
        bottomNavigation = (AHBottomNavigation) view.findViewById(R.id.bottom_navigation);
        homePager = (RtlViewPager) view.findViewById(R.id.homePager);
    }
}

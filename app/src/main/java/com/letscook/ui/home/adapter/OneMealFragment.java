package com.letscook.ui.home.adapter;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.letscook.R;
import com.letscook.model.Meal;
import com.letscook.ui.base.BaseFragment;
import com.letscook.ui.home.IngrAdapter;
import com.letscook.utils.ViewUtils;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;
import kotlin.jvm.internal.Intrinsics;

import static android.content.Context.MODE_PRIVATE;

public class OneMealFragment extends BaseFragment implements IngrAdapter.IngrInterface {
    private IngrAdapter adapter;
    @BindView(R.id.ingRV)
    RecyclerView ingRV;
    @BindView(R.id.name)
    TextView nameTV;
    @BindView(R.id.cookET)
    TextView cook;
    @BindView(R.id.img)
    ImageView img;
    private ArrayList<Meal> ings = new ArrayList<>();
    public OneMealFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one_meal, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    Meal meal;
    @Override
    protected void setUp(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            meal = (Meal) bundle.getSerializable("meal");
            if (meal != null) {
                nameTV.setText(meal.getName());
                cook.setText(meal.getDesc());
                if (meal.getImg() != null) {
                    Glide.with(this).load(meal.getImg()).into(img);
                }
            }
            assert meal != null;
            ings=new ArrayList<>(meal.getIngs().values());
            Log.e("meal",ings.toString());
            adapter = new IngrAdapter(ings, this::ingrInterface);
            setUpRecyclers(ingRV);
        }

    }

    public static OneMealFragment newInstance(@NotNull String title,Meal meal) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        Bundle bundle = new Bundle();
        bundle.putSerializable("meal", meal);
        OneMealFragment fragment = new OneMealFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);recyclerView.addItemDecoration((new GridSpacingItemDecoration(2, ViewUtils.dpToPx(4.0F), true)));
    }

    @Override
    public void ingrInterface(Meal meal) {

    }
}

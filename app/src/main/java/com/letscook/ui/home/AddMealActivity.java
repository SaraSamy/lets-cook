package com.letscook.ui.home;

import android.Manifest;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.letscook.R;
import com.letscook.model.Meal;
import com.letscook.ui.base.BaseActivity;
import com.letscook.ui.main.MainContract;
import com.letscook.utils.ViewUtils;
import com.marcoscg.dialogsheet.DialogSheet;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;
import gun0912.tedbottompicker.TedRxBottomPicker;
import kotlin.jvm.internal.Intrinsics;

import static androidx.core.content.ContextCompat.checkSelfPermission;
import static java.security.AccessController.getContext;

public class AddMealActivity extends BaseActivity implements  IngrAdapter.IngrInterface {

    IngrAdapter adapter;
    IngrAdapter selectedAdapter;
    @BindView(R.id.imageChooser)
    ImageView imageChooser;
    @BindView(R.id.nameET)
    EditText nameET;
    @BindView(R.id.cookET)
    EditText cookET;
    @BindView(R.id.mealIV)
    ImageView mealIV;
    @BindView(R.id.selectIng)
    CardView selectIng;
    @BindView(R.id.ingRV)
    RecyclerView selectedRV;
    @BindView(R.id.addB)
    Button addB;

    private static final int MY_PERMISSIONS_REQUEST = 100;
    String image = "https://firebasestorage.googleapis.com/v0/b/findmeal-f3c90.appspot.com/o/vegetable.png?alt=media&token=6a3521ed-cf91-4c36-815e-0b43013f3f74";
    private ArrayList<Meal> ings = new ArrayList<>();
    private ArrayList<Meal> selectedIng = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_meal);
        ButterKnife.bind(this);
        adapter = new IngrAdapter(ings, this::ingrInterface);
        selectedAdapter = new IngrAdapter(selectedIng, this::ingrInterface);
        GridLayoutManager girdLayoutManager = new GridLayoutManager(AddMealActivity.this, 2);
        selectedRV.setLayoutManager(girdLayoutManager);
        selectedRV.setAdapter(selectedAdapter);
        selectedRV.addItemDecoration((new GridSpacingItemDecoration(2, ViewUtils.dpToPx(4.0F), true)));

        showLoading();
        FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference()
                .child("ings").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    ings.clear();
                    for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                        Meal meal = childDataSnapshot.getValue(Meal.class);
                        meal.setId(childDataSnapshot.getKey());
                        ings.add(meal);
                    }
                    adapter.addIngrs(ings);
                    hideLoading();
                } else
                    Log.e("ings", "empty");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(2, ViewUtils.dpToPx(4.0F), true)));
    }

    void chooseIngBS() {
        DialogSheet dialogSheet = new DialogSheet(this);
        dialogSheet.setTitle("Ingredients")
                .setColoredNavigationBar(true)
                .setView(R.layout.add_meal_bottom_sheet)
                .setTitleTextSize(20)
                .setButtonsColorRes(R.color.colorAccent)
                .show();
        View inflatedView = dialogSheet.getInflatedView();
        Button add = inflatedView.findViewById(R.id.addB);
        RecyclerView ingRV = inflatedView.findViewById(R.id.ingRV);
        setUpRecyclers(ingRV);
        add.setOnClickListener(view -> {
            dialogSheet.dismiss();
        });
    }

    int i=0;

    private void getImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (  ActivityCompat.checkSelfPermission(this,Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED &&

                    ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED &&

                    ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST);
                return;
            }
        }
        TedRxBottomPicker.with(this)
                .show()

                .subscribe(uri -> {
                            image = uri.toString();
                            if (mealIV != null)
                                Glide.with(this)
                                        .load(image)
                                        .into(mealIV);
                        },
                        throwable -> {
                        });
    }

    @Override
    public void ingrInterface(Meal meal) {
        selectedIng.add(meal);
        selectedAdapter.addIngrs(selectedIng);
        Toast.makeText(this,meal.getName()+" added",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void setUp() {
    }

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, AddMealActivity.class);
        return intent;
    }

    @Override
    protected void onResume() {
        super.onResume();
        selectIng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseIngBS();
            }
        });
        imageChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImage();
                Glide.with(AddMealActivity.this)
                        .load(image)
                        .into(mealIV);
            }
        });

        addB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            showLoading();
                FirebaseStorage.getInstance()
                        .getReference(System.currentTimeMillis() + ".jpg").putFile(Uri.parse(image)).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        taskSnapshot.getMetadata()
                                .getReference()
                                .getDownloadUrl()
                                .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                           @Override
                                                           public void onComplete(@NonNull Task<Uri> task) {
                                                               String key = FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference()
                                                                       .child("meals").push().getKey();
                                                               FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference()
                                                                       .child("meals").child(key).setValue(
                                                                       new Meal(
                                                                               nameET.getText().toString(),
                                                                               null,
                                                                               Objects.requireNonNull(task.getResult()).toString(),
                                                                               cookET.getText().toString()
                                                                       )
                                                               ).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                   @Override
                                                                   public void onComplete(@NonNull Task<Void> task) {
                                                                       for (Meal ing : selectedIng) {
                                                                           FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference()
                                                                                   .child("meals").child(key).child("ings").child(ing.getId()).setValue(ing).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                               @Override
                                                                               public void onComplete(@NonNull Task<Void> task) {
                                                                                   if (i == selectedIng.size()-1) {
                                                                                     hideLoading();
                                                                                     Toast.makeText(AddMealActivity.this, "added", Toast.LENGTH_SHORT).show();
                                                                                   } else {
                                                                                       i++;
                                                                                   }
                                                                               }
                                                                           });
                                                                       }
                                                                   }
                                                               });
                                                           }
                                                       }
                                );
                    }
                });

            }
        });
    }
}

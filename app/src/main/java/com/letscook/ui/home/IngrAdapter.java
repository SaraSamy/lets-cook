package com.letscook.ui.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.letscook.R;
import com.letscook.model.Meal;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IngrAdapter extends RecyclerView.Adapter<IngrVH> {

    private ArrayList<Meal> ingrs;
    private IngrInterface ingrInterface;

    public IngrAdapter(ArrayList<Meal> ingrs, IngrInterface ingrInterface) {
        this.ingrs = ingrs;
        this.ingrInterface=ingrInterface;
    }

    @Override
    public IngrVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new IngrVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_meal, null));
    }

    @Override
    public void onBindViewHolder(IngrVH holder, int position) {
        holder.bind(ingrs.get(position));
        holder.itemView.setOnClickListener(view -> {
            ingrInterface.ingrInterface(ingrs.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return ingrs.size();
    }

    public void addIngrs(ArrayList< Meal> notifications1) {
        if (notifications1 != null) {
            notifyDataSetChanged();
        }
    }

    public ArrayList< Meal> getIngrs() {
        return ingrs;
    }

    public interface IngrInterface {
        void ingrInterface(Meal meal);
    }
}


class IngrVH extends RecyclerView.ViewHolder {

    @BindView(R.id.mealTV)
    TextView titleTV;
    @BindView(R.id.mealIV)
    ImageView mealIV;

    IngrVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind( Meal meal) {
        titleTV.setText(meal.getName());
        Glide.with(itemView.getContext()).load(meal.getImg()).into(mealIV);
    }
}


package com.letscook.ui.home;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.letscook.R;
import com.letscook.model.Meal;
import com.letscook.ui.base.BaseFragment;
import com.letscook.ui.home.adapter.MealsAdapter;
import com.letscook.ui.home.adapter.OneMealFragment;
import com.letscook.utils.ViewUtils;
import com.marcoscg.dialogsheet.DialogSheet;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;
import gun0912.tedbottompicker.TedRxBottomPicker;
import kotlin.jvm.internal.Intrinsics;

import static android.content.Context.MODE_PRIVATE;
import static androidx.core.content.ContextCompat.checkSelfPermission;

public class HomeFragment extends BaseFragment implements MealsAdapter.MealI {
    MealsAdapter adapter;
    @BindView(R.id.mealsRV)
    RecyclerView mealsRV;
    @BindView(R.id.addFab)
    FloatingActionButton addFab;
    @BindView(R.id.suggMeal)
    Button suggMeal;
    private ArrayList<Meal> meals = new ArrayList<>();

    @Override
    public void onStart() {
        super.onStart();
        setUpRecyclers(mealsRV);
        SharedPreferences prefs = Objects.requireNonNull(getActivity()).getSharedPreferences("letsCook", MODE_PRIVATE);
        String type = prefs.getString("type", "type");
        if (type.equals("admin")) {
            suggMeal.setVisibility(View.GONE);
        }
        else{
            addFab.setVisibility(View.GONE);
        }
        getBaseActivity().showLoading();
        FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference()
                .child("meals").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    meals.clear();
                    for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                        Meal meal = childDataSnapshot.getValue(Meal.class);
                        meal.setId(childDataSnapshot.getKey());
                        meals.add(meal);
                    }
                    adapter.addMealsVHs(meals);

                }
                getBaseActivity().hideLoading();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        adapter = new MealsAdapter(meals, this::MealI);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void setUp(View view) {
        addFab.setOnClickListener(view1 -> {
            startActivity(AddMealActivity.getStartIntent(getContext()));
        });
        suggMeal.setOnClickListener(view1 -> {
            startActivity(SugMealsActivity.getStartIntent(getContext()));
        });

    }

    public static HomeFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new HomeFragment();
    }

    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(2, ViewUtils.dpToPx(4.0F), true)));
    }


    @Override
    public void MealI(Meal meal) {
        getBaseActivity().replaceFragmentToActivity(OneMealFragment.newInstance(" meal", meal), true);
//        startActivity(OneMealActivity.getStartIntent(getContext(),meal));
    }


}

package com.letscook.ui.home;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.letscook.R;
import com.letscook.model.Meal;
import com.letscook.ui.base.BaseActivity;
import com.letscook.ui.home.adapter.OneMealFragment;
import com.letscook.ui.main.MainActivity;

import butterknife.ButterKnife;

public class OneMealActivity extends BaseActivity {

    @Override
    protected void setUp() {
        replaceFragmentToActivity(new OneMealFragment(), true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUp();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    public static Intent getStartIntent(Context context, Meal meal) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("meal", meal);
        Intent intent= new Intent();

        return new Intent(context, MainActivity.class);
    }
}

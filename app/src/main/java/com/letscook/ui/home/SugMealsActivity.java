package com.letscook.ui.home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.letscook.R;
import com.letscook.model.Meal;
import com.letscook.ui.base.BaseActivity;
import com.letscook.ui.home.adapter.MealsAdapter;
import com.letscook.utils.ViewUtils;
import com.marcoscg.dialogsheet.DialogSheet;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;
import gun0912.tedbottompicker.TedRxBottomPicker;

public class SugMealsActivity extends BaseActivity implements  MealsAdapter.MealI {

    MealsAdapter adapter;
    @BindView(R.id.mealsRV)
    RecyclerView mealsRV;
    private ArrayList<Meal> meals = new ArrayList<>();
    private ArrayList<Meal> myIngs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sug_meals_fragment);
        ButterKnife.bind(this);
        adapter = new MealsAdapter(meals, this::MealI);
        setUpRecyclers(mealsRV);
    }

    @Override
    protected void setUp() {
      }

    @Override
    protected void onStart() {
        super.onStart();
        showLoading();
        FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference()
                .child("store").child(FirebaseAuth.getInstance().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    Meal ing = childDataSnapshot.getValue(Meal.class);
                    ing.setId(childDataSnapshot.getKey());
                    myIngs.add(ing);
                }
                FirebaseDatabase.getInstance("https://letscook-1846a-default-rtdb.firebaseio.com/").getReference()
                        .child("meals").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            meals.clear();
                            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                Meal meal = childDataSnapshot.getValue(Meal.class);
                                meal.setId(childDataSnapshot.getKey());
                                for(Meal ing : myIngs){
                                    Log.e("testtt",String.valueOf(meal.getIngs().containsKey(ing.getId())));
                                    if(meal.getIngs().containsKey(ing.getId())){
                                        meals.add(meal);
                                        break;
                                    }
                                }
                            }
                            adapter.addMealsVHs(meals);
                            hideLoading();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(2, ViewUtils.dpToPx(4.0F), true)));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUp();
    }

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, SugMealsActivity.class);
        return intent;
    }

    @Override
    public void MealI(Meal meal) {

    }
}

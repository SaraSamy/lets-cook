package com.letscook.ui.home.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.letscook.R;
import com.letscook.model.Meal;
import java.util.ArrayList;

public class MealsAdapter extends RecyclerView.Adapter<MealVH> {

    private ArrayList<Meal> meals;
    private MealI mealI;

    public MealsAdapter(ArrayList<Meal> meals, MealI mealI) {
        this.meals = meals;
        this.mealI=mealI;
    }

    @Override
    public MealVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MealVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_meal, null));
    }

    @Override
    public void onBindViewHolder(MealVH holder, int position) {
        holder.bind(meals.get(position));
        holder.itemView.setOnClickListener(view -> {
            mealI.MealI(meals.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return meals.size();
    }

    public void addNotificationVH(Meal notification) {
        if (notification != null) {
            meals.add(notification);
            notifyDataSetChanged();
        }
    }


    public void addMealsVHs(ArrayList< Meal> notifications1) {
        if (notifications1 != null) {
            notifyDataSetChanged();
        }
    }

    public ArrayList< Meal> getMeals() {
        return meals;
    }

    public interface MealI {
        void MealI(Meal meal);
    }
}

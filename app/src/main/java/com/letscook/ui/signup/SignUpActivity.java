package com.letscook.ui.signup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.google.gson.Gson;
import com.letscook.R;
import com.letscook.data.AppDataManager;
import com.letscook.model.User;
import com.letscook.ui.base.BaseActivity;
import com.letscook.ui.main.MainActivity;

public class SignUpActivity extends BaseActivity implements SignUpContract.View {

    SignUpPresenter presenter;
    protected Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setUp();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void setUp() {
        presenter = new SignUpPresenter(new AppDataManager());
        presenter.onAttach(this);
    }


    @Override
    public void onSignUpSuccess( User user) {
        SharedPreferences.Editor editor = getSharedPreferences("voter", MODE_PRIVATE).edit();
        editor.putString("type", "user");
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString("user", json);
        editor.apply();
        Intent signUp = new Intent(SignUpActivity.this, MainActivity.class);
        signUp.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(signUp);
        finish();
    }


    public void signup(View view) {

        EditText email=findViewById(R.id.email);
        EditText password=findViewById(R.id.password);
        EditText confirmPassword=findViewById(R.id.confirmPassword);
        EditText phone = findViewById(R.id.phone);
        EditText name = findViewById(R.id.name);

        if (email.getText().toString().isEmpty()) {
            email.setError("Email is required");
            email.requestFocus();
            return;
        }
        if (name.getText().toString().isEmpty()) {
            name.setError("name is required");
            name.requestFocus();
            return;
        }
        if (phone.getText().toString().isEmpty()) {
            phone.setError("Phone is required");
            phone.requestFocus();
            return;
        }
        if (password.getText().toString().isEmpty()) {
            password.setError("Password is required");
            password.requestFocus();
            return;
        }
        if (password.length() < 6) {
            confirmPassword.setError("Password can not be less than 6 characters");
            confirmPassword.requestFocus();
            return;
        }
        if (confirmPassword.getText().toString().isEmpty()||!(confirmPassword.getText().toString().equals(password.getText().toString()))) {
            confirmPassword.setError("confirm password must match your password");
            confirmPassword.requestFocus();
            return;
        }
//        if (!location.isChecked()) {
//            location.setError("location must be checked");
//        }
        else{
//            getLocation();
            presenter.signUp(email.getText().toString().trim(),password.getText().toString(),new User(
                    name.getText().toString(),
                    "https://firebasestorage.googleapis.com/v0/b/photographer-62d5a.appspot.com/o/account.png?alt=media&token=df2975b7-72b3-4a3e-ab9c-43b0b3849011",
                    null,
                    "user",
                    phone.getText().toString(),
                    "",
                    email.getText().toString()

            ));
        }
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, SignUpActivity.class);
    }
}

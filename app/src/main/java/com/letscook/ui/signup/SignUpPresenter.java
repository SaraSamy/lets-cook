package com.letscook.ui.signup;

import com.letscook.R;
import com.letscook.data.DataManager;
import com.letscook.model.User;
import com.letscook.ui.base.BaseLisener;
import com.letscook.ui.base.BasePresenter;
import com.letscook.utils.CommonUtils;

public class SignUpPresenter <V extends SignUpContract.View>
        extends BasePresenter<V> implements SignUpContract.Presenter<V>, BaseLisener< User, String,String> {

    public SignUpPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void signUp(String email, String password, User user) {

        if (email.isEmpty()) {
            getMvpView().showMessage(R.string.error_email_required);
            return;
        } else if (!CommonUtils.isEmailValid(email)) {
            getMvpView().showMessage(R.string.error_invalid_email);
            return;
        }
        if (password.isEmpty()) {
            getMvpView().showMessage(R.string.error_password_empty);
            return;
        }

        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().signUp(email, password, this,user);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }

    @Override
    public void onSuccess(User user) {
        getMvpView().hideLoading();
        if (isViewAttached())
            getMvpView().onSignUpSuccess(user);
    }

    @Override
    public void onSuccessData(User data, String key) {

    }

    @Override
    public void onFail(String error) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            getMvpView().showMessage(error);
        }
    }
}

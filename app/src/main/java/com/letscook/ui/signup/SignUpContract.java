package com.letscook.ui.signup;
import com.letscook.model.User;
import com.letscook.ui.base.MvpPresenter;
import com.letscook.ui.base.MvpView;

public interface SignUpContract {

    interface View extends MvpView {
        void onSignUpSuccess(User user);
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
        void signUp(String email, String password, User user);
    }
}

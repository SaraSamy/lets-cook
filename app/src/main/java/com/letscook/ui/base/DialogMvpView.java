
package com.letscook.ui.base;


public interface DialogMvpView extends MvpView {

    void dismissDialog(String tag);
}

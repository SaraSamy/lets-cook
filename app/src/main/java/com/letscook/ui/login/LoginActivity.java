package com.letscook.ui.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.letscook.R;
import com.letscook.data.AppDataManager;
import com.letscook.model.User;
import com.letscook.ui.base.BaseActivity;
import com.letscook.ui.main.MainActivity;
import com.letscook.ui.signup.SignUpActivity;

import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements LoginContract.View {

    EditText email;
    EditText password;
    LoginPresenter presenter;

    @Override
    protected void setUp() {
        setUnBinder(ButterKnife.bind(this));
    }

    public void goToSignUp(View view) {
        startActivity(SignUpActivity.getStartIntent(this));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        presenter = new LoginPresenter(new AppDataManager());
        presenter.onAttach(this);
    }

    public void onLogin(View view) {
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        if (email.getText().toString().isEmpty()) {
            email.setError("Email is required");
            email.requestFocus();
            return;
        }
        if (password.getText().toString().isEmpty()) {
            password.setError("Password is required");
            password.requestFocus();
            return;
        } else
            presenter.login(email.getText().toString().trim(), password.getText().toString());
    }

    @Override
    public void onLoginSuccess(User user) {
        Log.e("type",user.getType());
        SharedPreferences.Editor editor = getSharedPreferences("letsCook", MODE_PRIVATE).edit();
        editor.putString("type", user.getType());
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString("user", json);
        editor.apply();
        Intent login = new Intent(LoginActivity.this, MainActivity.class);
        login.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(MainActivity.getStartIntent(this));
        finish();
    }


    public static Intent getStartIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }
}

package com.letscook.ui.login;
import com.letscook.model.User;
import com.letscook.ui.base.MvpPresenter;
import com.letscook.ui.base.MvpView;

public interface LoginContract {

    public interface View extends MvpView {
        void onLoginSuccess(User user);
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
    void login(String email, String password);
    }
}
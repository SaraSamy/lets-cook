package com.letscook.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Meal implements Serializable {
    String name;
    String id;
    String img;
    String desc;
    List<Meal> ingMeal;
    Map<String,Meal> ings;

    public List<Meal> getIngMeal() {
        return ingMeal;
    }

    public void setIngMeal(List<Meal> ingMeal) {
        this.ingMeal = ingMeal;
    }

    public Map<String, Meal> getIngs() {
        return ings;
    }

    public void setIngs(Map<String, Meal> ings) {
        this.ings = ings;
    }

    public Meal(String name, String id, String img, String desc, List<Meal> ingMeal) {
        this.name = name;
        this.id = id;
        this.img = img;
        this.desc = desc;
        this.ingMeal = ingMeal;
    }

    @Override
    public String toString() {
        return "Meal{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", img='" + img + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }

    public Meal() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Meal(String name, String id, String img, String desc) {
        this.name = name;
        this.id = id;
        this.img = img;
        this.desc = desc;
    }
}

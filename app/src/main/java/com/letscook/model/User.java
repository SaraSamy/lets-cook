package com.letscook.model;
import java.io.Serializable;

public class User implements Serializable {
    private String name;
    private String img = "https://firebasestorage.googleapis.com/v0/b/photographer-62d5a.appspot.com/o/account.png?alt=media&token=df2975b7-72b3-4a3e-ab9c-43b0b3849011";
    private String id;
    private String type;
    private String phone;
    private String email;

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", img='" + img + '\'' +
                ", id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    public User(String name, String img, String id, String type, String phone, String bio, String email) {
        this.name = name;
        this.img = img;
        this.id = id;
        this.type = type;
        this.phone = phone;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
